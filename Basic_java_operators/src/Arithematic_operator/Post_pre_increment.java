package Arithematic_operator;

public class Post_pre_increment {

	public static void main(String[] args) {
		//post increment
		int x = 4; 
		System.out.println(x);
		int y = x++;           //x=4, assigning first to y, so y=4; 
						       //then incrementing x -> 4+1=5,x=5
		System.out.println(y);
		int z = ++x;           //x=5, first incrementing x -> 5+1=6,x=6
		                       //then assigning to z, so z=6
		System.out.println(z);
		}

}
